import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println ("Input an integer whose factorial will be computed: ");
        Scanner in = new Scanner(System.in);

        try {
            int num = in.nextInt();

            if (num < 0) {
                System.out.println("Factorial is not defined for negative numbers.");
            } else {
                int factorial = 1;
                int counter = 1;

                while (counter <= num) {
                    factorial *= counter;
                    counter++;
                }
//                for (counter = 1; counter <= num; counter++) {
//                    factorial *= counter;
//                }

                System.out.println("The factorial of " + num + " is " + factorial);
            }
        }
        catch(Exception e){
            System.out.println("Invalid Input. Please enter a positive integer.");
            e.printStackTrace();
        }


    }
}